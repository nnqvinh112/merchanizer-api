﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class Eodetail
    {
        public string IdEo { get; set; }
        public string InfoProduct { get; set; }
        public int? Amount { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Cost { get; set; }
        public string AppliedPromotions { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public ExportOrder IdEoNavigation { get; set; }
        public Product InfoProductNavigation { get; set; }
    }
}
