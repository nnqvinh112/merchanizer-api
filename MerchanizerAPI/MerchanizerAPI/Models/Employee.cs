﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class Employee
    {
        public Employee()
        {
            ExportOrder = new HashSet<ExportOrder>();
            ImportOrder = new HashSet<ImportOrder>();
        }

        public string IdEm { get; set; }
        public string NameEm { get; set; }
        public string Phone { get; set; }
        public string Indentify { get; set; }
        public string Address { get; set; }
        public string Role { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public ICollection<ExportOrder> ExportOrder { get; set; }
        public ICollection<ImportOrder> ImportOrder { get; set; }
    }
}
