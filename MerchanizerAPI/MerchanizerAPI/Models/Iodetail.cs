﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class Iodetail
    {
        public string IdIo { get; set; }
        public string InfoProduct { get; set; }
        public int? Amount { get; set; }
        public decimal? Cost { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public ImportOrder IdIoNavigation { get; set; }
        public Product InfoProductNavigation { get; set; }
    }
}
