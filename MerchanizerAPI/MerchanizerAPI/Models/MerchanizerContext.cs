﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MerchanizerAPI.Models
{
    public partial class MerchanizerContext : DbContext
    {
        public MerchanizerContext()
        {
        }

        public MerchanizerContext(DbContextOptions<MerchanizerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Eodetail> Eodetail { get; set; }
        public virtual DbSet<ExportOrder> ExportOrder { get; set; }
        public virtual DbSet<ImportOrder> ImportOrder { get; set; }
        public virtual DbSet<Iodetail> Iodetail { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Promotion> Promotion { get; set; }
        public virtual DbSet<Supplier> Supplier { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-MUE9P6C;Initial Catalog=Merchanizer;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Email).HasColumnType("text");

                entity.Property(e => e.NameAc).HasColumnType("text");

                entity.Property(e => e.Password).HasColumnType("text");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.HasKey(e => e.IdCa);

                entity.Property(e => e.IdCa)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatdTime).HasColumnType("datetime");

                entity.Property(e => e.NameCa).HasColumnType("text");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.IdCus);

                entity.Property(e => e.IdCus)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address).HasColumnType("text");

                entity.Property(e => e.Birthday).HasColumnType("datetime");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Email).HasColumnType("text");

                entity.Property(e => e.Gender)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Identify).HasColumnType("text");

                entity.Property(e => e.LastVisitedTime).HasColumnType("datetime");

                entity.Property(e => e.NameCus).HasColumnType("text");

                entity.Property(e => e.Phone).HasColumnType("text");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.IdEm);

                entity.Property(e => e.IdEm)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address).HasColumnType("text");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Indentify).HasColumnType("text");

                entity.Property(e => e.NameEm).HasColumnType("text");

                entity.Property(e => e.Phone).HasColumnType("text");

                entity.Property(e => e.Role).HasColumnType("text");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });

            modelBuilder.Entity<Eodetail>(entity =>
            {
                entity.HasKey(e => new { e.IdEo, e.InfoProduct });

                entity.ToTable("EODetail");

                entity.Property(e => e.IdEo)
                    .HasColumnName("IdEO")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.InfoProduct)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.AppliedPromotions)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Cost).HasColumnType("money");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Discount).HasColumnType("money");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.HasOne(d => d.IdEoNavigation)
                    .WithMany(p => p.Eodetail)
                    .HasForeignKey(d => d.IdEo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EOD_EO");

                entity.HasOne(d => d.InfoProductNavigation)
                    .WithMany(p => p.Eodetail)
                    .HasForeignKey(d => d.InfoProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EOD_PRO");
            });

            modelBuilder.Entity<ExportOrder>(entity =>
            {
                entity.HasKey(e => e.IdEo);

                entity.Property(e => e.IdEo)
                    .HasColumnName("IdEO")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.IdCus)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.TotalCost).HasColumnType("money");

                entity.Property(e => e.TotalDiscount).HasColumnType("money");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ExportOrder)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_EO_EM");

                entity.HasOne(d => d.IdCusNavigation)
                    .WithMany(p => p.ExportOrder)
                    .HasForeignKey(d => d.IdCus)
                    .HasConstraintName("FK_EO_CU");
            });

            modelBuilder.Entity<ImportOrder>(entity =>
            {
                entity.HasKey(e => e.IdIo);

                entity.Property(e => e.IdIo)
                    .HasColumnName("IdIO")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.SuppliedBy)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.TotalCost).HasColumnType("money");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ImportOrder)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_IO_Em");

                entity.HasOne(d => d.SuppliedByNavigation)
                    .WithMany(p => p.ImportOrder)
                    .HasForeignKey(d => d.SuppliedBy)
                    .HasConstraintName("FK_IO_Sup");
            });

            modelBuilder.Entity<Iodetail>(entity =>
            {
                entity.HasKey(e => new { e.IdIo, e.InfoProduct });

                entity.ToTable("IODetail");

                entity.Property(e => e.IdIo)
                    .HasColumnName("IdIO")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.InfoProduct)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Cost).HasColumnType("money");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.HasOne(d => d.IdIoNavigation)
                    .WithMany(p => p.Iodetail)
                    .HasForeignKey(d => d.IdIo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IOD_IO");

                entity.HasOne(d => d.InfoProductNavigation)
                    .WithMany(p => p.Iodetail)
                    .HasForeignKey(d => d.InfoProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IOD_PRO");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => e.IdPro);

                entity.Property(e => e.IdPro)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Category)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Image).HasColumnType("image");

                entity.Property(e => e.NamePro).HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.HasOne(d => d.CategoryNavigation)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.Category)
                    .HasConstraintName("FK_PRO_CATE");
            });

            modelBuilder.Entity<Promotion>(entity =>
            {
                entity.HasKey(e => e.IdProm);

                entity.Property(e => e.IdProm)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.DiscountType).HasColumnType("text");

                entity.Property(e => e.DiscountedProduct)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.EndTime).HasColumnType("date");

                entity.Property(e => e.NameProm).HasColumnType("text");

                entity.Property(e => e.StartTime).HasColumnType("date");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");

                entity.HasOne(d => d.DiscountedProductNavigation)
                    .WithMany(p => p.Promotion)
                    .HasForeignKey(d => d.DiscountedProduct)
                    .HasConstraintName("FK_PROM_PRO");
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.HasKey(e => e.IdSup);

                entity.Property(e => e.IdSup)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasColumnType("text");

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.Name).HasColumnType("text");

                entity.Property(e => e.Phone).HasColumnType("text");

                entity.Property(e => e.UpdatedTime).HasColumnType("datetime");
            });
        }
    }
}
