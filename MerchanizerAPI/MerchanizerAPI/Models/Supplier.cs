﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class Supplier
    {
        public Supplier()
        {
            ImportOrder = new HashSet<ImportOrder>();
        }

        public string IdSup { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int? Taxcode { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public ICollection<ImportOrder> ImportOrder { get; set; }
    }
}
