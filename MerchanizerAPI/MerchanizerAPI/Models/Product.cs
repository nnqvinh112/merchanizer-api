﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class Product
    {
        public Product()
        {
            Eodetail = new HashSet<Eodetail>();
            Iodetail = new HashSet<Iodetail>();
            Promotion = new HashSet<Promotion>();
        }

        public string IdPro { get; set; }
        public string NamePro { get; set; }
        public decimal? Price { get; set; }
        public int? Quantity { get; set; }
        public byte[] Image { get; set; }
        public string ImageUrl { get; set; }
        public string Category { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public Category CategoryNavigation { get; set; }
        public ICollection<Eodetail> Eodetail { get; set; }
        public ICollection<Iodetail> Iodetail { get; set; }
        public ICollection<Promotion> Promotion { get; set; }
    }
}
