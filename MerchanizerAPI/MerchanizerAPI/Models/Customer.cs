﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class Customer
    {
        public Customer()
        {
            ExportOrder = new HashSet<ExportOrder>();
        }

        public string IdCus { get; set; }
        public string NameCus { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Identify { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public DateTime? Birthday { get; set; }
        public DateTime? LastVisitedTime { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public ICollection<ExportOrder> ExportOrder { get; set; }
    }
}
