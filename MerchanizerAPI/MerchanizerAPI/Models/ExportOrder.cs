﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class ExportOrder
    {
        public ExportOrder()
        {
            Eodetail = new HashSet<Eodetail>();
        }

        public string IdEo { get; set; }
        public string IdCus { get; set; }
        public int? TotalAmount { get; set; }
        public decimal? TotalDiscount { get; set; }
        public decimal? TotalCost { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public Employee CreatedByNavigation { get; set; }
        public Customer IdCusNavigation { get; set; }
        public ICollection<Eodetail> Eodetail { get; set; }
    }
}
