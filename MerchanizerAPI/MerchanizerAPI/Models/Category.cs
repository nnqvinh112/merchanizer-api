﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class Category
    {
        public Category()
        {
            Product = new HashSet<Product>();
        }

        public string IdCa { get; set; }
        public string NameCa { get; set; }
        public DateTime? CreatdTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public ICollection<Product> Product { get; set; }
    }
}
