﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class Account
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string NameAc { get; set; }
        public string Email { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
    }
}
