﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class ImportOrder
    {
        public ImportOrder()
        {
            Iodetail = new HashSet<Iodetail>();
        }

        public string IdIo { get; set; }
        public int? TotalAmount { get; set; }
        public decimal? TotalCost { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public string SuppliedBy { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public Employee CreatedByNavigation { get; set; }
        public Supplier SuppliedByNavigation { get; set; }
        public ICollection<Iodetail> Iodetail { get; set; }
    }
}
