﻿using System;
using System.Collections.Generic;

namespace MerchanizerAPI.Models
{
    public partial class Promotion
    {
        public string IdProm { get; set; }
        public string NameProm { get; set; }
        public string Description { get; set; }
        public int? DiscountValue { get; set; }
        public string DiscountedProduct { get; set; }
        public string DiscountType { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public Product DiscountedProductNavigation { get; set; }
    }
}
