﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchanizerAPI.Models;

namespace MerchanizerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExportOrdersController : ControllerBase
    {
        private readonly MerchanizerContext _context;

        public ExportOrdersController(MerchanizerContext context)
        {
            _context = context;
        }

        // GET: api/ExportOrders
        [HttpGet]
        public IEnumerable<ExportOrder> GetExportOrder()
        {
            var query = (from m in _context.ExportOrder
                         join dm in _context.Employee on m.CreatedBy equals dm.IdEm
                         join cm in _context.Customer on m.IdCus equals cm.IdCus
                         select new ExportOrder()
                         {
                             IdEo = m.IdEo,
                             Eodetail = m.Eodetail,
                             CreatedTime = m.CreatedTime,
                             UpdatedTime = m.UpdatedTime,
                             TotalAmount = m.TotalAmount,
                             TotalCost = m.TotalCost,
                             TotalDiscount = m.TotalDiscount,
                             Status = m.Status,
                             CreatedBy = m.CreatedBy,
                             CreatedByNavigation = dm,
                             IdCus = m.IdCus,
                             IdCusNavigation = cm
                         }).ToList();
            return query;
        }

        // GET: api/ExportOrders/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetExportOrder([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exportOrder = await (from m in _context.ExportOrder
                                     where m.IdEo == id
                                     join dm in _context.Employee on m.CreatedBy equals dm.IdEm
                                     join cm in _context.Customer on m.IdCus equals cm.IdCus
                                     select new ExportOrder()
                                     {
                                         IdEo = m.IdEo,
                                         Eodetail = m.Eodetail,
                                         CreatedTime = m.CreatedTime,
                                         UpdatedTime = m.UpdatedTime,
                                         TotalAmount = m.TotalAmount,
                                         TotalCost = m.TotalCost,
                                         TotalDiscount = m.TotalDiscount,
                                         Status = m.Status,
                                         CreatedBy = m.CreatedBy,
                                         CreatedByNavigation = dm,
                                         IdCus = m.IdCus,
                                         IdCusNavigation = cm,
                                     }).FirstOrDefaultAsync();

            if (exportOrder == null)
            {
                return NotFound();
            }

            var details = await _context.Eodetail.Where(i => i.IdEo.Trim() == id).ToListAsync();
            foreach (Eodetail iDetails in details)
            {
                var product = await _context.Product
                    .Where(x => x.IdPro.Trim() == iDetails.InfoProduct.Trim())
                    .Select(x => new Product { NamePro = x.NamePro, Image = x.Image, IdPro = x.IdPro, ImageUrl = x.ImageUrl })
                    .FirstOrDefaultAsync();
                iDetails.InfoProductNavigation = product;
            }
            exportOrder.Eodetail = details;
            return Ok(exportOrder);
        }

        // PUT: api/ExportOrders/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutExportOrder([FromRoute] string id, [FromBody] ExportOrder exportOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != exportOrder.IdEo)
            {
                return BadRequest();
            }

            _context.Entry(exportOrder).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExportOrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ExportOrders
        [HttpPost]
        public async Task<IActionResult> PostExportOrder([FromBody] ExportOrder exportOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ExportOrder.Add(exportOrder);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ExportOrderExists(exportOrder.IdEo))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetExportOrder", new { id = exportOrder.IdEo }, exportOrder);
        }

        // DELETE: api/ExportOrders/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteExportOrder([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exportOrder = await _context.ExportOrder.FindAsync(id);
            if (exportOrder == null)
            {
                return NotFound();
            }

            _context.ExportOrder.Remove(exportOrder);
            await _context.SaveChangesAsync();

            return Ok(exportOrder);
        }

        private bool ExportOrderExists(string id)
        {
            return _context.ExportOrder.Any(e => e.IdEo == id);
        }
    }
}