﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchanizerAPI.Models;

namespace MerchanizerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IodetailsController : ControllerBase
    {
        private readonly MerchanizerContext _context;

        public IodetailsController(MerchanizerContext context)
        {
            _context = context;
        }

        // GET: api/Iodetails
        [HttpGet]
        public IEnumerable<Iodetail> GetIodetail()
        {
            return _context.Iodetail;
        }

        // GET: api/Iodetails/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetIodetail([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var iodetail = await _context.Iodetail.FindAsync(id);

            if (iodetail == null)
            {
                return NotFound();
            }

            return Ok(iodetail);
        }

        // PUT: api/Iodetails/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIodetail([FromRoute] string id, [FromBody] Iodetail iodetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != iodetail.IdIo)
            {
                return BadRequest();
            }

            _context.Entry(iodetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IodetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Iodetails
        [HttpPost]
        public async Task<IActionResult> PostIodetail([FromBody] Iodetail iodetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Iodetail.Add(iodetail);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (IodetailExists(iodetail.IdIo))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetIodetail", new { id = iodetail.IdIo }, iodetail);
        }

        // DELETE: api/Iodetails/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteIodetail([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var iodetail = await _context.Iodetail.FindAsync(id);
            if (iodetail == null)
            {
                return NotFound();
            }

            _context.Iodetail.Remove(iodetail);
            await _context.SaveChangesAsync();

            return Ok(iodetail);
        }

        private bool IodetailExists(string id)
        {
            return _context.Iodetail.Any(e => e.IdIo == id);
        }
    }
}