﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MerchanizerAPI.Models;

namespace MerchanizerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImportOrdersController : ControllerBase
    {
        private readonly MerchanizerContext _context;

        public ImportOrdersController(MerchanizerContext context)
        {
            _context = context;
        }

        // GET: api/ImportOrders
        [HttpGet]
        public IEnumerable<ImportOrder> GetImportOrder()
        {
            var query = (from m in _context.ImportOrder
                         join em in _context.Employee on m.CreatedBy equals em.IdEm
                         join sm in _context.Supplier on m.SuppliedBy equals sm.IdSup
                         select new ImportOrder()
                         {
                             IdIo = m.IdIo,
                             Iodetail = m.Iodetail,
                             CreatedTime = m.CreatedTime,
                             UpdatedTime = m.UpdatedTime,
                             TotalAmount = m.TotalAmount,
                             TotalCost = m.TotalCost,
                             Status = m.Status,
                             SuppliedBy = m.SuppliedBy,
                             SuppliedByNavigation = sm,
                             CreatedBy = m.CreatedBy,
                             CreatedByNavigation = em,
                         }).ToList();
            return query;
        }

        // GET: api/ImportOrders/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetImportOrder([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var importOrder = await (from m in _context.ImportOrder
                                     where m.IdIo == id
                                     join dm in _context.Employee on m.CreatedBy equals dm.IdEm
                                     join sm in _context.Supplier on m.SuppliedBy equals sm.IdSup
                                     select new ImportOrder()
                                     {
                                         IdIo = m.IdIo,
                                         CreatedTime = m.CreatedTime,
                                         UpdatedTime = m.UpdatedTime,
                                         TotalAmount = m.TotalAmount,
                                         TotalCost = m.TotalCost,
                                         Status = m.Status,
                                         CreatedBy = m.CreatedBy,
                                         SuppliedBy = m.SuppliedBy,
                                         SuppliedByNavigation = sm,
                                         CreatedByNavigation = dm,
                                     }).FirstOrDefaultAsync();

            if (importOrder == null)
            {
                return NotFound();
            }

            var details = await _context.Iodetail.Where(i => i.IdIo.Trim() == id).ToListAsync();
            foreach (Iodetail iDetails in details)
            {
                var product = await _context.Product
                    .Where(x => x.IdPro.Trim() == iDetails.InfoProduct.Trim())
                    .Select(x => new Product { NamePro = x.NamePro, Image = x.Image, IdPro = x.IdPro })
                    .FirstOrDefaultAsync();
                iDetails.InfoProductNavigation = product;
            }
            importOrder.Iodetail = details;
            return Ok(importOrder);
        }

        // PUT: api/ImportOrders/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutImportOrder([FromRoute] string id, [FromBody] ImportOrder importOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != importOrder.IdIo)
            {
                return BadRequest();
            }

            _context.Entry(importOrder).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ImportOrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ImportOrders
        [HttpPost]
        public async Task<IActionResult> PostImportOrder([FromBody] dynamic body)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var details = body["products"];
            var importOrder = new ImportOrder();
            _context.ImportOrder.Add(body);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ImportOrderExists(body.IdIo))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetImportOrder", new { id = body.IdIo }, body);
        }

        // DELETE: api/ImportOrders/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteImportOrder([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var importOrder = await _context.ImportOrder.FindAsync(id);
            if (importOrder == null)
            {
                return NotFound();
            }

            _context.ImportOrder.Remove(importOrder);
            await _context.SaveChangesAsync();

            return Ok(importOrder);
        }

        private bool ImportOrderExists(string id)
        {
            return _context.ImportOrder.Any(e => e.IdIo == id);
        }
    }
}